package com.amitbansal7.typeahead.model;

import org.springframework.data.mongodb.core.mapping.Document;

//This Class contains a word along with its frequency

@Document(collection="products_item")
public class Suggestion implements Comparable<Suggestion> {

	private String name;
	private Integer freq;

	public Suggestion(String name) {
		this.name = name;
		freq = 1;
	}

	public void incrementFreq() {
		this.freq++;
	}

	public String getWord() {
		return name;
	}

	public Integer getFreq() {
		return freq;
	}

	@Override
	public int compareTo(Suggestion that) {
		return that.getFreq() - this.freq;
	}
}
